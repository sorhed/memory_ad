﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Debug = System.Diagnostics.Debug;

namespace Assets.Scripts
{
    public class ManuController : MonoBehaviour
    {
        [SerializeField]//ten atrybut oznacza ze mozemy podlaczac obiekty za pomoca edytora unity
        private GameObject selectLevelPanel, mainMenuPanel, gamePanel, nextButtonImage, hsPanel;//panele menu podlaczane za pomoca edytora

        [SerializeField]
        private Animator selectLevelAnim, mainMenuAnim, gameAnim, hsAnim;//komponenty przechowujace animacje powyzszych paneli

        [SerializeField]
        private Sprite lockText, unlockText;//tekstury kłodki i strzałki na przycisku który przenosi nas do kolejnego levelu

        private readonly Dictionary<string, NumberOfCards> levelSelectDict = new Dictionary<string, NumberOfCards>();//słownik który łączy nam nazwy przycisków do wyboru poziomów z enumem używanym do określenia ilości kart w danym etapie (na jego podstawie skrypt dealer rozdaje karty)
        private string levelName; //nazwa levelu na podstawie której wysyłamy odpowiedniego enuma przy uzyciu powyzszego slownika
        private GameObject currentGameObject; //przechowujemy tu dane o obecne kliknietym obiekcie (button np.)
        private bool isNextLocked = true; //czy przycisk przenoszacy nas do nastepnego levelu jest odblokowany czy nie

        public void OnButtonDown() //ta funkcja wykonuje sie po kliknieciu w przycisk wyboru etapu
        {
            if (levelSelectDict.Count == 0)
            {
                levelSelectDict.Add("Lev1", NumberOfCards.Six); // deklaracja słownika
                levelSelectDict.Add("Lev2", NumberOfCards.Eight);
                levelSelectDict.Add("Lev3", NumberOfCards.Ten);
                levelSelectDict.Add("Lev4", NumberOfCards.Twelve);
            }

            currentGameObject = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject; // przyporzadkowanie obecnego obiektu do zmiennej 
            levelName = currentGameObject.name; //wyciagamy nazwe z obecnie kliknietego obiektu
            StartCoroutine(HideSelectPanel()); //puszczamy sekwencję, która odpala animację chowania poszczególnego panela i odpalania kolejnego
        }

        public void OnStartButtonDown()//funkcja działająca po kliknięciu przycisku "start"
        {
            StartCoroutine(HideMainPanel()); //sekwencja chowająca panel głownego menu
            StartCoroutine(ShowSelectPanel()); //sekwencja pokazująca panel menu wyboru etapu
        }

        public void OnHsButtonDown() //funkcja włączająca się po kliknięciu przycisku ""HighScores"
        {
            StartCoroutine(HideMainPanel()); //sekwencja chowająca główny panel
            StartCoroutine(ShowHsPanel()); //sekwencja pokazująca panel highscoreów
            onHsShow(); //event dzięki któremu tablica highscore'ów się zapełnia
        }

        public void OnBackButtonHS() //przycisk powrót z poziomu tabeli wyników
        {
            StartCoroutine(HideHsPanel()); //chowa menu wyników
            StartCoroutine(ShowMainPanel()); //pokazuje główne menu
        }

        public void OnBackButtonGame()//przycisk powrót z poziomu ekrany gry
        {
            StartCoroutine(HideGamePanel()); //chowa panel gry
            StartCoroutine(ShowMainPanel()); //pokazuje główne menu
            OnGameInterrupted(); //wysyła event na wieść o kórym inne skrypty resetują zegar, niszczą obecnie rozdane karty i ogólnie resetują stan gry

        }

        public void OnScoresSaved()//funkcja wywoływana po zapisaniu wyników na koniec etapu
        {
            var level = FindObjectOfType<Dealer>().numofCards;

            if (level != NumberOfCards.Twelve)
            {
                levelName = levelSelectDict.Keys.FirstOrDefault(key => levelSelectDict[key] == (NumberOfCards)((int)level + 2)); //przypisuje level o dwie karty "wyższy" od obecnego do przycisku next 
                StartCoroutine(UnlockNextButton()); //odblokowuje przycisk next
            }
        }

        public void OnNextButton()//funkcja wywoływana po kliknięciu przycisku next
        {
            if (!isNextLocked) //jeśli nie jest odblokowany
            {
                StartCoroutine(PlayNextLevel()); //włącza nowy level
            }
        }
        //to poniższe to sekwencje - umożliwiają one puszczanie animacji i czekanie określoną ilość czasu tak, by animacje były widoczne. Te użyte tu są raczej łatwe, głównie służą do robienia przejść między menusami, etc.
        private IEnumerator UnlockNextButton()
        {
            nextButtonImage.GetComponent<Image>().sprite = unlockText;
            yield return new WaitForSeconds(0.3f);
            isNextLocked = false;
        }

        private IEnumerator LockNextButton()
        {
            nextButtonImage.GetComponent<Image>().sprite = lockText;
            yield return new WaitForSeconds(0.3f);
            isNextLocked = true;
        }

        private IEnumerator ShowHsPanel()
        {
            hsPanel.SetActive(true);
            hsAnim.Play("SlideIn");
            yield return new WaitForSeconds(1.1f);
        }

        private IEnumerator HideHsPanel()
        {
            hsAnim.Play("SlideOut");
            yield return new WaitForSeconds(1.1f);
            hsPanel.SetActive(false);
        }

        private IEnumerator ShowMainPanel()
        {
            mainMenuPanel.SetActive(true);
            mainMenuAnim.Play("SlideIn");
            yield return new WaitForSeconds(1.1f);
        }

        private IEnumerator HideMainPanel()
        {
            mainMenuAnim.Play("SlideOut");
            yield return new WaitForSeconds(1.1f);
            mainMenuPanel.SetActive(false);
            
        }

        private IEnumerator ShowSelectPanel()
        {
            selectLevelPanel.SetActive(true);
            selectLevelAnim.Play("SlideIn");
            yield return new WaitForSeconds(1.1f);
        }

        private IEnumerator HideGamePanel()
        {
            gameAnim.Play("SlideOut");
            yield return new WaitForSeconds(1.1f);
            gamePanel.SetActive(false);
        }

        private IEnumerator HideSelectPanel()
        {
            selectLevelAnim.Play("SlideOut");
            yield return new WaitForSeconds(1.1f);
            selectLevelPanel.SetActive(false);
            gamePanel.SetActive(true);
            gameAnim.Play("SlideIn");
            yield return new WaitForSeconds(1.1f);
            onLevelStarted();
            StartCoroutine(LockNextButton());
        }

        private IEnumerator PlayNextLevel()
        {
            gameAnim.Play("SlideOut");
            yield return new WaitForSeconds(1.1f);
            gamePanel.SetActive(false);
            gamePanel.SetActive(true);
            gameAnim.Play("SlideIn");
            yield return new WaitForSeconds(1.1f);
            OnGameInterrupted();
            onLevelStarted();
            StartCoroutine(LockNextButton());
        }

        //poniżej funkcje odpalające eventy

        private void onLevelStarted() //event który lecina rozpoczęcie levelu (żeby dealer mógł rozdać karty)
        {
            if (LevelStarted != null)
                LevelStarted.Invoke(levelSelectDict[levelName]);
        }

        private void onHsShow() //event który powiadamia inny skrypt żeby pokazać HighScore'y w tabeli wyników
        {
            if (HsShow == null)
                return;
            HsShow.Invoke();
        }

        private void OnGameInterrupted() //event do restartu stanu gry
        {
            if (GameInterrupted == null)
                return;
            GameInterrupted.Invoke();
        }

        public event Action<NumberOfCards> LevelStarted;
        public event Action HsShow, GameInterrupted;
    }
}
