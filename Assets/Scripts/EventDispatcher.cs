﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class EventDispatcher : MonoBehaviour
    {
        public List<Card> SubscribedCards;
        public Action<Card> SubscribeCard;
        public Action<Card> UnsubscribeCard;
        [SerializeField] private Dealer dealer;
        [SerializeField] private CardComparer cardComparer;
        [SerializeField] private ShowTime showTime;
        [SerializeField] private ManuController menuController;
        [SerializeField] private HighScoresController hsController;


        void Start()
        {
            SubscribedCards = new List<Card>();
            SubscribeCard = card => SubscribedCards.Add(card);
            UnsubscribeCard = card => SubscribedCards.Remove(card);
        }


        void Update() //tutaj łączymy eventy z poszczególnymi funkcjami
        {
           foreach (var card in SubscribedCards)
            {
                card.Clicked -= cardComparer.GetGuess;
                card.Clicked += cardComparer.GetGuess;

                cardComparer.WasGuessed -= card.Guessed;
                cardComparer.WasGuessed += card.Guessed;

                cardComparer.BadGuessed -= card.NotGuessed;
                cardComparer.BadGuessed += card.NotGuessed;

                dealer.GameStop -= card.DestroyMe;
                dealer.GameStop += card.DestroyMe;

                menuController.GameInterrupted -= card.DestroyMe;
                menuController.GameInterrupted += card.DestroyMe;
            }


            dealer.GameStop -= showTime.SwitchOffCounter;
            dealer.GameStop += showTime.SwitchOffCounter;

            dealer.GameStarted -= showTime.SwitchOnCounter;
            dealer.GameStarted += showTime.SwitchOnCounter;

            dealer.GameStop -= hsController.OnGameStop;
            dealer.GameStop += hsController.OnGameStop;

            hsController.ScoresSaved -= menuController.OnScoresSaved;
            hsController.ScoresSaved += menuController.OnScoresSaved;

            menuController.HsShow -= hsController.OnHsShow;
            menuController.HsShow += hsController.OnHsShow;

            menuController.LevelStarted -= dealer.StartTheGame;
            menuController.LevelStarted += dealer.StartTheGame;

            menuController.GameInterrupted -= showTime.Reset;
            menuController.GameInterrupted += showTime.Reset;

            menuController.GameInterrupted -= dealer.OnGameReset;
            menuController.GameInterrupted += dealer.OnGameReset;

        }


    }
}

