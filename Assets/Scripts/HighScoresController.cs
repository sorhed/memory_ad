﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class HighScoresController : MonoBehaviour
    {

        [SerializeField] private GameObject hsPanel;//okienko zapisu wyników
        [SerializeField] private Animator hsAnim;//animacje tego okienka
        [SerializeField] private Text uiText, hsView; //odpowiednio tekst na okienku do zapisywania wyników i tekst na oknie do pokazywania wyników
        [SerializeField] private InputField inputField; //okno w którym użytkownik wpisuje swoje imie
        private ShowTime showTime;

        private string playerName; //imię gracza
        private float playerTime; //czas gracza

        void Init()//funkcja wrzucana na rozruch
        {
            showTime = FindObjectOfType<ShowTime>(); //uzyskuje dostęp do skryptu liczącego czas
            playerTime = showTime.TimeSinceDealt; //czyta obecny czas gracza
            uiText.text = "Twój wynik to: " + playerTime; //pokazuje tekst w okienku z wynikami
        }

        public void OnGameStop()//gdy gra jest skończona:
        {
            Init(); //populujemy zmienne panelu do zapisu wyników
            StartCoroutine(ShowThePanel()); //pokazujemy panel do pokazywania wyników
        }

        public void OnInputEnded() //ta funkcja odpala się gdy skończymy wprowadzać dane do okienka zapisu wyników
        {
            var level = FindObjectOfType<Dealer>().numofCards; //zczytujemy ze skryptu dealer ile kart było w granym obecnie levelu
            playerName = inputField.text; //zczytujemy imie gracza z okienka do wpisywania tekstu
            var currentString = PlayerPrefs.GetString("HighScores"); //wyciągamy z playerprefsów obecne highscore'y (unity pozwala przechowywać w player prefsach niewielkie ilości danych, wykorzystałem je tu do przechowywania wyników)
            PlayerPrefs.SetString("HighScores", currentString + Environment.NewLine + "Ilosc kart: " + (int)level + ", Gracz: " + playerName + " " + playerTime); //zapisuje wyniki jako jeden string, zapisując równiez na jakim etapie dany czas został uzyskany
            StartCoroutine(Saved()); //powiadamia gracza, ze wyniki zostaly zapisane i usuwa okienko
            OnScoresSaved(); //wysyła event na wieść o którym odblokowuje się przycisk next (można przejść do kolejnego etapu)
        }

        public void OnHsShow()
        {
           hsView.text = PlayerPrefs.GetString("HighScores");
        }

        private IEnumerator Saved()
        {
            uiText.text = "Dzieki, " + playerName + " zapisano twoj wynik!";
            yield return new WaitForSeconds(1f);
            StartCoroutine(HideThePanel());
        }

        private IEnumerator ShowThePanel()
        {
            hsPanel.SetActive(true);
            hsAnim.Play("popUp");
            yield return new WaitForSeconds(0.5f);
        }

        private IEnumerator HideThePanel()
        {
            hsAnim.Play("popDown");
            yield return new WaitForSeconds(0.5f);
            hsPanel.SetActive(false);
        }

        private void OnScoresSaved()
        {
            if (ScoresSaved != null)
                ScoresSaved.Invoke();
        }

        public event Action ScoresSaved;
    }
}
