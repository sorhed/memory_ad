﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class Card : MonoBehaviour
    {
        [SerializeField] private EventDispatcher eventDispatcher;
        public bool IsFlipped { get; set; } //czy karta jest obrócona
        public bool IsActive { get; set; } //czy karta jest aktywna (nieodgadnięta)
        public CardType ThisCardType { get; private set; } //określa typ danej karty
        public int RotationTime = 1; //określa prędkość z jaką karta ma się obracać, można zmienić wartość

        public void Initialize(CardType cardType) //pseudo konstruktor, daje typ danej karcie i zaznacza ją jako nieodgadniętą na początku rozgrywki
        {
            ThisCardType = cardType;
            IsActive = true;
            eventDispatcher = FindObjectOfType<EventDispatcher>();
            StartCoroutine(Switch());//przy inicjalizacji każdej karty zwołuje sekwencję, która obraca kartę i potem obraca ponownie
        }

        void Update() //co klatkę, jeśli dana karta nie jest zapisana na listę subskrybentów eventów, dodaje ją tam
        {
            if (!eventDispatcher.SubscribedCards.Contains(this))
                eventDispatcher.SubscribeCard(this);
        }

        private void OnMouseUp() //funkcja wywoływana jeśli karta zostanie kliknięta
        {
            if (IsActive && !IsFlipped)
            {
                    GetComponent<AudioSource>().Play(); //odpala dźwięk obracania się
                    RotateMe(); //odpala funkcję obracającą kartą
            }
        }

        public void RotateMe() //funkcja obracająca
        {
            Debug.Log("Rotate!");
            iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(-90, 0, 0), "time", RotationTime, "onStart", "Flip", "onComplete", "OnClicked")); //obraca kartę, wywołując przy tym funkcję OnClicked po zakończeniu obrotu i Flip przed rozpoczęciem obrotu
        }

        private IEnumerator Switch() //sekwencja obracająca karty na początku gry
        {
            iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(-90, 0, 0), "time", 0.8, "onStart", "Flip"));
            yield return new WaitForSeconds(1.1f);//zmieniając tę wartość ustalamy jak długo ma być odwrócona dana karta
            iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(-90, -180, 0), "time", 0.8, "onStart", "Flip"));
            yield return new WaitForSeconds(1.1f);
        }

        public void DestroyMe() //funkcja która usuwa kartę ze zbioru kart odpowiadających na eventy w skrypcie EventDispatcher oraz niszczy sama siebie (przy przedwczesnym wyjściu z gry np.)
        {
            if (eventDispatcher.SubscribedCards.Contains(this))
            {
                eventDispatcher.UnsubscribeCard(this);
                Destroy(gameObject);
            }
        }

        void Flip() //funkcja zmieniająca wartość właściwości "OnFlip"
        {
            IsFlipped = !IsFlipped;
        }

        public void NotGuessed(Card card) //funkcja odpowiadająca na event wskazujący na nieodgadnięcie karty; karta obraca się wierzchem do góry (wraca do pierwotnej pozycji)
        {
            if (card == this)
            {
                iTween.RotateTo(gameObject, iTween.Hash("rotation", new Vector3(-90, -180, 0), "time", RotationTime, "onComplete", "Flip")); //rotacja wywołująca funkcję Flip po zakończeniu obrotu
            }
        }

        public void Guessed(Card card) //funkcja odpowiadająca na event wskazujący na odgadnięcie kart; karta staje się nieaktywna i nie reaguje na kliknięcia
        {
            if (card == this)
            {
                IsActive = false;
            }
        }

        public void OnClicked() //funkcja wywołująca event który jest odbierany przez CardComparera powodujący dodanie karty do kolejki do sprawdzenia czy jest odgadnięta; funkcja ta wywoływana jest z funkcji OnMouseUp
        {
            if (Clicked == null)
                return;

            Clicked.Invoke(this);
        }

        public event Action<Card> Clicked;
    }
}
