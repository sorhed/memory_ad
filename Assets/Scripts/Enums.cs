﻿using UnityEngine;

namespace Assets.Scripts
{
    public enum CardType
    {
        Card0 = 0,
        Card1 = 1,
        Card2 = 2,
        Card3 = 3,
        Card4 = 4,
        Card5 = 5
    }

    public enum NumberOfCards
    {
        Six = 6,
        Eight = 8,
        Ten = 10,
        Twelve = 12
    }
}
