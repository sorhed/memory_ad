﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class ShowTime : MonoBehaviour
    { 
        private float timeSinceDealt;
        private bool isStarted;
        public float TimeSinceDealt
        {
            get
            {
                return timeSinceDealt;
            }
        }


        // Use this for initialization
        void Start()
        {
        }

        void FixedUpdate()
        {
            if (!isStarted) //jeśli gra się nie zaczęła, nie wykonuj dalej tej funkcji
                return;

            timeSinceDealt += Time.time; //dodaj czas do licznika czasu co jedną klatkę
        }

        // Update is called once per frame
        void OnGUI()
        { 
                GUI.Label(new Rect(0, 0, 100, 50),timeSinceDealt.ToString()); //wyświetl czas w górnym rogu planszy
        }

        public void SwitchOnCounter() //reaguje na wywołanie eventu, włącza naliczanie się czasu
        {
            isStarted = true;
        }

        public void SwitchOffCounter() //jw. tylko wyłącza
        {
            isStarted = false;
        }

        public void Reset() // funkcja która resetuje stoper
        {
            timeSinceDealt = 0;
            isStarted = false;
        }
    }
}
