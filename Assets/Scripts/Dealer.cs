﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class Dealer : MonoBehaviour {

        public GameObject Card0, Card1, Card2, Card3, Card4, Card5; 
        private readonly List<Vector3>[] positionsArray = new List<Vector3>[4];
        private Dictionary<CardType, GameObject> cardDictionary;
        private List<Card> DeltCards = new List<Card>();
        public NumberOfCards numofCards;
        private bool cardsDealt;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (cardsDealt)
            {
                OnGameStarted();
                cardsDealt = false;
            }

            if (DeltCards.TrueForAll(card => card.IsActive == false) && DeltCards.Count != 0)
            {
                OnGameStop();
                DeltCards.Clear();
            } //jeśli wszystkie karty są nieaktywne (zgadnięte) to wysyłany jest event OnGameStop (czyli gra została 'wygrana')             
        }

        public void StartTheGame(NumberOfCards numberOfCards)
        {
            numofCards = numberOfCards;
            GetPositions(); //tu wywołujemy funkcję która zapełnia listę pozycji kart w zależności od ich ilości
            cardDictionary = new Dictionary<CardType, GameObject> //tu mapujemy rodzaje kart z ich prefabami (obiektami gotowych kart które są w edytorze w folderze 'prefabs'
            {
                {CardType.Card0, Card0},
                {(CardType) 1, Card1},
                {(CardType) 2, Card2},
                {(CardType) 3, Card3},
                {(CardType) 4, Card4},
                {(CardType) 5, Card5}
            };

            switch (numofCards) //w zależności od tego ile kart ma się rozdać tyle jest rozdawanych (wywoływana jest funkcja dealCards z odpowiednimi parametrami. Używana jest również fukncja GetRandomList(ilość niepowtarzających się kart) która zwraca potasowane typy kart
            {
                case NumberOfCards.Six:
                    DeltCards = dealCards(GetRandomList(3), positionsArray[0], NumberOfCards.Six).ToList();
                    break;

                case NumberOfCards.Eight:
                    DeltCards = dealCards(GetRandomList(4), positionsArray[1], NumberOfCards.Eight).ToList();
                    break;

                case NumberOfCards.Ten:
                    DeltCards = dealCards(GetRandomList(5), positionsArray[2], NumberOfCards.Ten).ToList();
                    break;

                case NumberOfCards.Twelve:
                    DeltCards = dealCards(GetRandomList(6), positionsArray[3], NumberOfCards.Twelve).ToList();
                    break;
            }
            cardsDealt = true;
        }

        private IEnumerable<Card> dealCards(IEnumerable<CardType> typeList, IEnumerable<Vector3> positions, NumberOfCards numberOfCards) //funkcja rozdająca karty
        {
            for (int i = 0; i < (int)numberOfCards; i++) //pętla mająca powtórzeń tyle, ile kart ma być rozdanych
            {
                var insta = (GameObject)Instantiate(cardDictionary[typeList.ElementAt(i)], positions.ElementAt(i), Quaternion.Euler(new Vector3(-90, -180, 0))); //wywołujemy prefaba i kładziemy go na określonej pozycji. Quaternion.Euler definiuje w jakiej pozycji karta się pojawia na stole
                var card = insta.GetComponent<Card>();
                card.Initialize(typeList.ElementAt(i)); //wywołujemy pseudo konstruktor poszczególnej karty i określamy jakiego typu jest dana karta
                yield return card; //dodajemy kartę do listy kart
            }
        }

        private List<CardType> GetRandomList(int numberOfSingleCards) //funkcja wywołująca potasowaną listę typów kart która później zostanie użyta do rozdawania
        {
            var intList = new List<int>();

            for (int i = 0; i < numberOfSingleCards; i++) //dodajemy do kolekcji dwie sztuki każdej niepowtarzającej się karty
            {
                intList.Add(i);
                intList.Add(i);
            }
            intList.Shuffle(); //tasujemy listę

            return intList.Select(value => (CardType)value).ToList(); //zwracamy listę
        }

        private void GetPositions()
        {
            positionsArray[0] = new List<Vector3>
            {
                new Vector3(-4f, 3f, 0),
                new Vector3(0f, 3f, 0),
                new Vector3(4f, 3f),
                new Vector3(-4f, -1f),
                new Vector3(0f, -1f),
                new Vector3(4f, -1f, 0)
            };

            positionsArray[1] = new List<Vector3>
            {
                new Vector3(-5.75f, 3, 0),
                new Vector3(-2.75f, 3, 0),
                new Vector3(0.25f, 3, 0),
                new Vector3(3.25f, 3, 0),
                                    
                new Vector3(-5.75f,-1, 0),
                new Vector3(-2.75f,-1, 0),
                new Vector3(0.25f, -1, 0),
                new Vector3(3.25f, -1, 0)
            };

            positionsArray[2] = new List<Vector3>
            {
                new Vector3(-5.75f, 3, 0),

                new Vector3(-2.75f, 4.8f, 0),
                new Vector3(0.25f, 4.8f, 0),

                new Vector3(3.25f, 3f, 0),
                new Vector3(3.25f, -1f, 0),

                new Vector3(0.25f, -2f, 0),
                new Vector3(-2.75f, -2f, 0),

                new Vector3(0.25f, 1.5f, 0),
                new Vector3(-2.75f, 1.5f, 0),

                new Vector3(-5.75f, -1f, 0)
            };

            positionsArray[3] = new List<Vector3>
            {
                new Vector3(-5.75f, 4.8f,0),
                new Vector3(-2.75f, 4.8f,0),
                new Vector3(0.25f, 4.8f,0),
                new Vector3(3.25f, 4.8f,0),
                                    
                new Vector3(-5.75f,1.5f,0),
                new Vector3(-2.75f,1.5f,0),
                new Vector3(0.25f, 1.5f,0),
                new Vector3(3.25f, 1.5f,0),
                                  
                new Vector3(-5.75f,-2f,0),
                new Vector3(-2.75f,-2f,0),
                new Vector3(0.25f, -2,0),
                new Vector3(3.25f, -2,0)
            };                     
        }

        public void OnGameReset()
        {
            DeltCards.Clear();
        }

        private void OnGameStarted()
        {
            if (GameStarted == null)
                return;

            GameStarted.Invoke();
        }

        private void OnGameStop()
        {
            if (GameStop == null)
                return;
            GameStop.Invoke();
        }     

        public event Action GameStop, GameStarted;
    }
}

