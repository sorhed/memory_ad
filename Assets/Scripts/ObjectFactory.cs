﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObjectFactory : MonoBehaviour
    {
        protected static ObjectFactory instance;
        public GameObject Card0, Card1, Card2, Card3, Card4, Card5;
        public Dictionary<CardType, GameObject> CardDictionary;

        void Start()
        {
            instance = this;
            CardDictionary = new Dictionary<CardType, GameObject>
            {
                {CardType.Card0, Card0},
                {(CardType) 1, Card1},
                {(CardType) 2, Card2},
                {(CardType) 3, Card3},
                {(CardType) 4, Card4},
                {(CardType) 5, Card5}
            };

        }

        public static Card CreateCard(Vector3 position, CardType cardType)
        {
            var insta = (GameObject) Instantiate(instance.CardDictionary[cardType], position, Quaternion.Euler(new Vector3(-90, -180)));
            var card = insta.GetComponent<Card>();
               card.Initialize(cardType);return card;
        }
    }
}
