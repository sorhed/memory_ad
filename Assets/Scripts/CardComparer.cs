﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class CardComparer : MonoBehaviour
    {
        public List<Card> CardsToCompare = new List<Card>();

        public void GetGuess(Card card)
        {
            if (card.IsActive)
            {
                CardsToCompare.Add(card); //dodaj kartę która została kliknięta do listy jeśli jest aktywna
                if (CardsToCompare.Count == 2) //jeśli dwie karty są dodane do listy wykonuj dalej
                {
                    if (CardsToCompare.ElementAt(0).ThisCardType == CardsToCompare.ElementAt(1).ThisCardType && CardsToCompare.ElementAt(1) != CardsToCompare.ElementAt(0)) //jeśli dwie kliknięte karty mają ten sam typ wykonuj dalje
                    {
                        Debug.Log("Good guess");
                        CardsToCompare.ForEach(card1 => onWasGuessed(card1)); //wywołaj event, karty zostały dobrze odgadnięte
                    }
                    else
                    {
                        Debug.Log("BadGuess");
                        CardsToCompare.ForEach(card1 => onBadGuessed(card1)); //wywołaj event, karty zostały źle odgadnięte
                    }
                 CardsToCompare.Clear();
                }
                if (CardsToCompare.Count > 2) // jeśli na liście jest więcej niż dwie karty [wyrzuca błąd, teoretycznie kod nie pozwala na taki przypadek]
                {
                    //should be unreachable
                    throw new ArgumentOutOfRangeException("Too many cards are flipped");
                }
            }
        }

      void onWasGuessed(Card card) //funkcja wywołująca event
        {
            if (WasGuessed == null)
                return;

            WasGuessed.Invoke(card);
        }

        void onBadGuessed(Card card) //jw.
        {
            if (BadGuessed == null)
                return;

            BadGuessed.Invoke(card);
        }



        public event Action<Card> WasGuessed , BadGuessed; //deklaracja eventów

    }
}
